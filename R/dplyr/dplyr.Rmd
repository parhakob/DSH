---
title: "R data manipulation with RStudio and dplyr: an introduction"
author: "Stéphane Guillou"
date: "`r Sys.Date()`"
output: github_document
---

## What are we going to learn?

In this hands-on session, you will use RStudio and the `dplyr` package to manipulate your data.

Specifically, you will learn how to **explore, filter, reorganise and process** your data with the following verbs:

* `filter()`: pick observations
* `arrange()`: reorder rows
* `select()`: pick variables
* `mutate()`: create new variables
* `summarise()`: collapse to a single summary
* `group_by()`: change the scope of function

## Keep in mind

* Everything we write today will be saved in your project. Please remember to save it in your H drive or USB if you are using a Library computer.
* R is case sensitive: it will tell the difference between uppercase and lowercase.
* Respect the naming rules for objects (no spaces, does not start with a number...)

### Help

For any dataset or function doubts that you might have, don't forget the three ways of getting help in RStudio:

1. the shortcut command: `?functionname`
2. the help function: `help(functionname)`
3. the keyboard shortcut: press F1 after writing a function name

## Open RStudio

* If you are using your own laptop please open RStudio
  * If you want to review the installation instructions: https://gitlab.com/stragu/DSH/blob/master/R/Installation.md
* Make sure you have a working internet connection
* On Library computers (the first time takes about 10 min.): 
    * Log in with your UQ credentials (student account if you have two)
    * Make sure you have a working internet connection
    * Go to search at bottom left corner (magnifiying glass) 
    * Open the ZENworks application
    * Look for RStudio
    * Double click on RStudio which will install both R and RStudio

## Setting up

### Install the dplyr package

If you don't have it already, you can install dplyr with the command: `install.packages("dplyr")`

> At home, you can install the whole "[tidyverse](https://www.tidyverse.org/)", a meta-package useful for data science: `install.packages("tidyverse")`

### New project

* Click the "File" menu button (top left corner), then "New Project"
* Click "New Directory"
* Click "New Project" ("Empty project" if you have an older version of RStudio)
* In "Directory name", type the name of your project, e.g. "dplyr_intro"
* Select the folder where to locate your project: for example, the `Documents/RProjects` folder, which you can create if it doesn't exist yet.
* Click the "Create Project" button

### Create a script

We will use a script to write code more comfortably.

* Menu: Top left corner, click the green "plus" symbol, or press the shortcut (for Windows/Linux) <kbd>Ctrl</kbd>+Shift</kbd>+N</kbd> or (for Mac) <kbd>Cmd</kbd>+<kbd>Shift</kbd>+<kbd>N</kbd>. This will open an "Untitled1" file.
* Go to "File > Save" or press (for Windows/Linux) <kbd>Ctrl</kbd>+<kbd>S</kbd> or (for Mac) <kbd>Cmd</kbd>+<kbd>S</kbd>. This will ask where you want to save your file and the name of the new file.
* Call your file "process.R"

We can now load the package to access its functions (remember you can use <kbd>Ctrl</kbd>+<kbd>shift</kbd> to execute a command from the script):

```{r}
library(dplyr)
```

> You only need to install a package once (with `install.packages()`), but you need to reload it every time you start a new R session (with `library()`).

### Introducing our data

#### Challenge 1 – import data

Let's import and explore our data.

1. read the data into an object called "gapminder", using `read.csv()`:

```{r}
gapminder <- read.csv("https://raw.githubusercontent.com/resbaz/r-novice-gapminder-files/master/data/gapminder-FiveYearData.csv")
```

2. Explore the gapminder dataset using `dim()` and `str()`

How can we get the dataframe's variable names? There are two ways: `names(gapminder)` returns the names regardless of the object type, such as list, vector, data.frame etc., whereas `colnames(gapminder)` returns the variable names for matrix-like objects, such as matrices, dataframes...

To return one specific column in the dataframe, you can use the dollar syntax: `gapminder$year`. For example, try these:

```{r}
nlevels(gapminder$country)
class(gapminder$country)
```

If we want a nicer display in the console, we can convert our dataframe to a "tibble". Try this:

```{r}
gapminder <- as_tibble(gapminder)
gapminder
```

## Basic dplyr verbs

The R package `dplyr` was developed by Hadley Wickham for data manipulation.

The book _[R for Data Science](http://r4ds.had.co.nz)_ introduces the package as follows:

> You are going to learn the five key dplyr functions that allow you to solve the vast majority of your data manipulation challenges:
> 
> * Pick observations by their values with `filter()`.
> * Reorder the rows with `arrange()`.
> * Pick variables by their names with `select()`.
> * Create new variables with functions of existing variables with `mutate()`.
> * Collapse many values down to a single summary with `summarise()`.
> 
> These can all be used in conjunction with `group_by()` which changes the scope of each function from operating on the entire dataset to operating on it group-by-group. These six functions provide the **verbs for a language of data manipulation**.

To use the verbs to their full extent, we will need **pipes** and **logical operators**, which we will introduce as we go.

### 1. Pick observations with `filter()`

The `filter()` function allows use to pick observations depending on one ore several conditions.

**Logical operators** allow us to **check for a condition**. Remember: `=` is used to pass on a value to an argument, whereas `==` is used to check for a condition.

* `==`: equal
* `!=`: different 
* `>`: greater than
* `<`: smaller than
* `>=`: greater or equal
* `<=`: smaller or equal

For example, filter the observations for Australia, using `filter()` and a logical operator:

```{r}
australia <- filter(gapminder, country == "Australia")
australia
```

Now, filter the rows that have a life expectancy `lifeExp` greater than 81 years:

```{r}
life81 <- filter(gapminder, lifeExp > 81)
dim(life81)
```

### 2. Reorder observations with `arrange()`

Arrange will reorder our rows according to a variable, by default in ascending order:

```{r}
arrange(life81, lifeExp)
```

If we want to have a look at the entries with highest life expectancy first, we can use the `desc()` function:

```{r}
arrange(life81, desc(lifeExp))
```

### 3. Pick variables with `select()`

Select allows us to pick variables (i.e. columns) from the dataset. For example, to only keep the data about year, country and GDP per capita:

```{r}
(gap_small <- select(gapminder, year, country, gdpPercap))
```

We wrap it in parentheses so it also prints to screen.

#### The pipe operator

If we only want this data for 1997, we can add a second step with the `filter()` verb:

```{r}
gap_small_97 <- filter(gap_small, year == 1997)
```

This is not ideal, as we create an intermediate object we don't necessarily want to keep.

We could do this in one single line by nesting the commands into each other:

```{r}
gap_small_97 <- filter(select(gapminder,
                              year, country, gdpPercap),
                       year == 1997)
```

... but this becomes very hard to read.

We can make our code more readable and avoid creating useless intermediate objects by **piping** commands into each other. The pipe operator `%>%` **strings commands together**, using the left side output as the first argument of the right side function.

For example, this command:

```{r}
summary(gapminder, maxsum = 10)
```

... is equivalent to:

```{r}
gapminder %>% summary(maxsum = 10)
```

Here's another example with the `filter()` verb:

```{r}
gapminder %>%
  filter(country != "France")
```

... becomes:

```{r}
filter(gapminder, country != "France")
```

To do what we did previously in one single command, using the pipe:

```{r}
gap_small_97 <- gapminder %>%
    select(year, country, gdpPercap) %>%
    filter(year == 1997)
```

The pipe operator can be read as "then" and makes the code a lot **more readable** than when nesting functions into each other, and avoids the creation of several intermediate objects.

From now on, we'll use this syntax.

#### Challenge 2 – a tiny dataset

Select the 2002 life expectancy observation for Eritrea (and remove the rest of the variables).

```{r}
eritrea_2002 <- gapminder %>%
    select(year, country, lifeExp) %>%
    filter(country == "Eritrea", year == 2002)
```

### 4. Create new variables with `mutate()`

Have a look at what the verb `mutate()` can do with `?mutate`.

Let's see what the two following variables can be used for:

```{r}
gapminder %>%
    select(gdpPercap, pop)
```

How do you think we could combine them to add something new to our dataset?

#### Challenge 3 – mutate the GDP

Use `mutate()` to create a `gdp` variable.

Name your new dataset `gap_gdp`. When finished, `dim(gap_gdp)` should result in `1704 7`.

Hint: use the `*` operator within `mutate()`.

```{r}
gap_gdp <- gapminder %>%
    mutate(gdp = gdpPercap * pop)
dim(gap_gdp)
head(gap_gdp)
```

You can reuse a variable computed by 'mutate()' straight away. For example, we also want a more readable version of our new variable, in million dollars:

```{r}
(gap_gdp <- gapminder %>%
    mutate(gdp = gdpPercap * pop, gdpMil = gdp / 10^6))
```

###  5. Collapse to a single value with `summarise()`

`summarise()` collapses many values down to a single summary. For example, to find the mean life expectancy for the whole dataset:

```{r}
gapminder %>%
  summarise(meanLE = mean(lifeExp))
```

### 6. Change the scope with `group_by()`

`group_by()` changes the scope of the following function(s) from operating on the entire dataset to operating on it group-by-group.

See the effect of the grouping step:

```{r}
gapminder %>%
    group_by(continent)
```

The data in the cells is the same, the size of the object is the same. However, the dataframe was converted to a **tibble**, because a dataframe is not capable of storing grouping information.

Using the `group_by()` function before summarising makes things more interesting. For example, to find out the total population per continent in 2007, we can do the following:

```{r}
gapminder %>% 
    filter(year == 2007) %>%
    group_by(continent) %>%
    summarise(pop = sum(pop))
```

#### Challenge 4 – max life expectancy

Group by country, and find out the maximum life expectancy ever recorded

Hint: `?max`

```{r}
gapminder %>% 
    group_by(country) %>%
    summarise(maxLE = max(lifeExp))
```

## More examples

Another example of new variable with `mutate()`, with a different dataset that dplyr provides:

```{r}
starwars %>% 
  mutate(bmi = mass / ((height / 100)  ^ 2)) %>%
  select(name:mass, bmi) # we can select ranges
```

And a more complex processing of a dataset:

```{r}
starwars %>%
  group_by(species) %>%
  summarise(
    n = n(), # this counts the number of rows in each group
    mass = mean(mass, na.rm = TRUE)
  ) %>%
  filter(n > 1)
```

An example of data manipulation and data visualisation in the same command:

```{r}
# increase in population per continent
library(ggplot2)
gapminder %>% 
  group_by(continent, year) %>% 
  summarise(pop = sum(pop)) %>% 
  ggplot(aes(x = year,
             y = pop,
             colour = continent)) +
  geom_line()
```

And another one, still using our gapminder dataset:

```{r}
# top and bottom variations in life expectancy
gapminder %>% 
  group_by(country) %>% 
  summarise(maxLifeExp = max(lifeExp),
            minLifeExp = min(lifeExp)) %>% 
  mutate(dif = maxLifeExp - minLifeExp) %>% 
  arrange(desc(dif)) %>% 
  slice(1:10, (nrow(.)-10):nrow(.)) %>% 
  ggplot(aes(x = reorder(country, dif), y = dif)) +
  geom_col() +
  coord_flip()
```

## Close project

If you want to close RStudio, make sure you save your script first.

You can then close the window, and if your script contains all the steps necessary for your data processing, it is safer to _not_ save your workspace at the prompt. It should only take a second te execute all the commands stored in your script when you re-open your project.

## What next?

Look at our compilation of resources: https://gitlab.com/stragu/DSH/blob/master/R/usefullinks.md